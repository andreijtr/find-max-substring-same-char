package test.java;

import main.java.logic.MaxSubstringFinder;
import main.java.logic.SubstringCount;
import org.junit.Assert;
import org.junit.Test;

public class MaxSubstringFinderTest {

    @Test
    public void findMaxSubstringTest(){

        MaxSubstringFinder maxSubstringFinder = new MaxSubstringFinder("bcaaaaezfff f3ppp");

        SubstringCount actual = maxSubstringFinder.findMaxSubstring();
        SubstringCount expected = new SubstringCount("a",4);

        Assert.assertEquals(expected,actual);
    }
}
