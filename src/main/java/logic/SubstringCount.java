package main.java.logic;

public class SubstringCount {

    //clasa ce contine un caracter si numarul sau de repetitii
    private String character;
    private int count;

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public SubstringCount(String character, int count) {
        this.character = character;
        this.count = count;
    }

    public SubstringCount() {}

    @Override                                                           //am facut override pe equals pt ca aveam nevoie sa compar doua obiecte SubstrCount
    public boolean equals(Object toCompare) {                           //in metoda Assert.assertequals de test. assert fol metoda equals si trebuie
                                                                        //specificat clar ce presupune egalitatea unor obiecte
        if (toCompare == this) {
            return true;
        }

        if (!(toCompare instanceof SubstringCount)) {
            return false;
        }

        SubstringCount castO = (SubstringCount) toCompare;

        return ((castO.count == this.count) && (castO.character.equals(this.character)));
    }
}
