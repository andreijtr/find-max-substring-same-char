package main.java.logic;

public class MaxSubstringFinder {

    private String word;                                         //propozitia din care vreau sa aflu cel mai lung substring

    public MaxSubstringFinder(String word) {
        this.word = word;
    }

    public SubstringCount findMaxSubstring() {

        SubstringCount maxSubstring = new SubstringCount();     // va returna un obiect ce contine caracterul si numarul de aparitii

        String[] wordToArray = word.split("");            //impart wordul pe caractere, iau in considerare inclusiv spatiile

        int i = 0;
        int j, count;

        while (i < wordToArray.length) {                        //parcurg arrayul
            count = 1;
            j = i + 1;

            while (j < wordToArray.length && wordToArray[j].equals(wordToArray[i])) {   //numar cate elemente sunt egale cu cel de la pozitia i
                count++;
                j++;
            }

            if(count >= maxSubstring.getCount()) {                                      //daca apare de mai multe ori decat maximul de pana acum, il inlocuiesc
                maxSubstring.setCharacter(wordToArray[i]);
                maxSubstring.setCount(count);
            }

            i = j;
        }

        return maxSubstring;
    }

}
