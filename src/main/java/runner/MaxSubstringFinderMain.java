package main.java.runner;

import main.java.logic.MaxSubstringFinder;
import main.java.logic.SubstringCount;

public class MaxSubstringFinderMain {

    public static void main(String[] args) {

        MaxSubstringFinder maxSubstringFinder = new MaxSubstringFinder("bcaaaaezfff f3ppp");

        SubstringCount largerSubstring = maxSubstringFinder.findMaxSubstring();

        System.out.println("character: " + largerSubstring.getCharacter() + "\ncount: " + largerSubstring.getCount());

    }
}
